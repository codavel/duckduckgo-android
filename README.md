### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying the Android App for DuckDuckGo, an open-source search engine emphasizing searchers' privacy.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/duckduckgo/Android).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/duckduckgo-android/-/compare/develop...codavel?from_project_id=32714608)) **TODO, update link on external project** :

1. Added maven repository and Codavel's Application ID and Secret to the app's build.gradle, required to download and use Codavel's SDK.
2. Start Codavel's Service when the main activity is created, so that our SDK can start processing the HTTP requests. 
3. Register our HTTP interceptor into the multiple app's OkHTTPClient (Network and Glide modules), so that all the HTTP requests executed through the app are processed and forwarded to our SDK.

